<?php 
/* refer here http://www.wpelixir.com/how-to-create-new-element-in-visual-composer/
Element Description: VC Hover Box
*/
 
// Element Class 
class vcInfoBox extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_infobox_mapping' ) );
        add_shortcode( 'vc_infobox', array( $this, 'vc_infobox_html' ) );
    }
     
    // Element Mapping
    public function vc_infobox_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
         
        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('VC Hoverbox', 'text-domain'),
                'base' => 'vc_infobox',
                'description' => __('SPH VC box', 'text-domain'), 
                'category' => __('SPH Custom Elements', 'text-domain'),   
                'icon' => get_template_directory_uri().'../assets/favicon512.png',            
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'text-domain' ),
                        'param_name' => 'title',
                        'value' => __( 'Default value', 'text-domain' ),
                        'description' => __( 'Enter your title here.', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),  
                     
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Background Image", "my-text-domain" ),
                        "param_name" => "image_url",
                        "value" => __( "", "my-text-domain" ),
                        'description' => __( 'Enter your Background Image.', 'text-domain' ),
                        'group' => 'Custom Group',
                    ),  

                    /*array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Text', 'text-domain' ),
                        'param_name' => 'text',
                        'value' => __( 'Default value', 'text-domain' ),
                        'description' => __( 'Box Text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),*/                  
                        
                ),
            )
        );                                
        
    }
     
    // Element HTML
    public function vc_infobox_html( $atts ) {
         
        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'image_url' => '',
                    /*'text' => '',*/
                ), 
                $atts
            )
        );
         
        // Fill $html var with data
        $imageSrc = wp_get_attachment_image_src($image_url, 'full');
        $html = '
        <div class="vk-sparta-image">
            
            <div class="vk-sparta-image-item">
                <div class="vk-sparta-item-img">
                    <a href="#">
                        <img src="' . $imageSrc[0] .'" alt="' . $atts['title'] . '" class="img-responsive"/>
                    </a>
                </div>
                <div class="vk-iamge-item-caption">
                    <h2>' . $title . '</h2>
                </div>
            </div>
            
        </div>';

        /*<div class="vc-infobox-text">' . $text . '</div>*/   
         
        return $html;
         
    }
     
} // End Element Class
 
 
// Element Class Init
new vcInfoBox();