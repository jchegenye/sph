<?php
/*
Element Description: VC Slider Box
*/
 
// Element Class 
class vcSliderBox extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_sliderbox_mapping' ) );
        add_shortcode( 'vc_sliderbox', array( $this, 'vc_sliderbox_html' ) );
    }
     
    // Element Mapping
    public function vc_sliderbox_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
         
        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('VC Sliderbox', 'text-domain'),
                'base' => 'vc_sliderbox',
                'description' => __('SPH VC slider box', 'text-domain'), 
                'category' => __('SPH Custom Elements', 'text-domain'),   
                'icon' => get_template_directory_uri().'../assets/favicon512.png',            
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Title', 'title-domain' ),
                        'param_name' => 'accomodation_title',
                        'value' => __( 'Master Room', 'title-domain' ),
                        'description' => __( 'Enter your title here.', 'title-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),

                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'desc-class',
                        'heading' => __( 'Description', 'text-domain' ),
                        'param_name' => 'accomodation_desc',
                        'value' => __( 'Brief room description - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'text-domain' ),
                        'description' => __( 'Enter your description here.', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),

                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Side Image", "my-image_url-domain" ),
                        "param_name" => "accomodation_image_url",
                        "value" => __( "", "my-image_url-domain" ),
                        'description' => __( 'Enter your Side Image.', 'image_url-domain' ),
                        'group' => 'Custom Group',
                    ),

                    array(
                        'type' => 'textfield',
                        'holder' => 'span',
                        'class' => 'bed-class',
                        'heading' => __( 'Bed', 'text-domain' ),
                        'param_name' => 'accomodation_bed',
                        'value' => __( '1 King Bed', 'text-domain' ),
                        'description' => __( 'Enter bed name here.', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 1,
                        'group' => 'Packages Group',
                    ),

                    array(
                        'type' => 'textfield',
                        'holder' => 'span',
                        'class' => 'view-class',
                        'heading' => __( 'View', 'view-domain' ),
                        'param_name' => 'accomodation_view',
                        'value' => __( 'Lake View', 'view-domain' ),
                        'description' => __( 'Enter view name here.', 'view-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Packages Group',
                    ),

                    array(
                        'type' => 'textfield',
                        'holder' => 'span',
                        'class' => 'size-class',
                        'heading' => __( 'Size', 'size-domain' ),
                        'param_name' => 'accomodation_size',
                        'value' => __( '50', 'size-domain' ),
                        'description' => __( 'Enter size here.', 'size-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Packages Group',
                    ), 

                    array(
                        'type' => 'textfield',
                        'holder' => 'span',
                        'class' => 'breakfirst-class',
                        'heading' => __( 'Bread First', 'breakfirst-domain' ),
                        'param_name' => 'accomodation_breakfirst',
                        'value' => __( 'Yes', 'breakfirst-domain' ),
                        'description' => __( 'Enter break first here.', 'breakfirst-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Packages Group',
                    ),

                    array(
                        'type' => 'textfield',
                        'holder' => 'span',
                        'class' => 'max-class',
                        'heading' => __( 'Max Accomodation', 'max-domain' ),
                        'param_name' => 'accomodation_max',
                        'value' => __( '2 Adults, 1 Child', 'max-domain' ),
                        'description' => __( 'Enter max accomodation here.', 'max-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Packages Group',
                    ),

                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'price-class',
                        'heading' => __( 'Price', 'price-domain' ),
                        'param_name' => 'accomodation_price',
                        'value' => __( '200', 'price-domain' ),
                        'description' => __( 'Enter price here.', 'price-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Packages Group',
                    ),        
                        
                ),
            )
        );                                
        
    }
     
    // Element HTML
    public function vc_sliderbox_html( $atts ) {
         
        // Params extraction
        extract(
            shortcode_atts(
                array(
                    
                    'accomodation_title'   => '',
                    'accomodation_desc' => '',
                    'accomodation_image_url' => '',
                    'accomodation_bed' => '',
                    'accomodation_view' => '',
                    'accomodation_size' => '',
                    'accomodation_breakfirst' => '',
                    'accomodation_max' => '',
                    'accomodation_price' => '',
                    
                ), 
                $atts
            )
        );
         
        // Fill $html var with data
        $imageSrc = wp_get_attachment_image_src($accomodation_image_url, 'full');
        $html = '

<div class="vk-sparta-dark-our-rooms-content">
    <div id="vk-owl-dark-our-rooms" class="vk-owl-one-item owl-carousel owl-theme">
        <div class="item">
            <div class="col-md-6 vk-dark-our-room-item-left  vk-clear-padding">
                <div class="vk-dark-our-room-item-img">
                    <a href="#"><img src="' . $imageSrc[0] .'" alt="' . $atts['title'] . '" class="img-responsive"/></a>
                </div>
            </div>
            <div class="col-md-6 vk-dark-our-room-item-right">
                <div class="vk-dark-our-room-item-content">
                    <h3><a href="#">'. $accomodation_title .'</a></h3>
                    <br>
                    <p>' . $accomodation_desc . '</p>
                    <ul>
                        <li><p><i class="fa fa-bed" aria-hidden="true"></i> Bed <span> : '.  $accomodation_bed .'</span></p></li>
                        <li><p><i class="fa fa-binoculars" aria-hidden="true"></i> View <span> : '. $accomodation_view .'</span></p></li>
                        <li><p><i class="fa fa-arrows-alt" aria-hidden="true"></i> Size <span> : '. $accomodation_size .' m2</span></p></li>
                        <li><p><i class="fa fa-coffee" aria-hidden="true"></i> Breakfast <span> : '. $accomodation_breakfirst .'</span></p></li>
                        <li><p><i class="fa fa-users" aria-hidden="true"></i> Max Occupancy: <span> : '. $accomodation_max .'</span></p></li>
                    </ul>
                    <div class="vk-dark-our-room-item-book">
                        <div class="vk-dark-our-room-item-book-left">
                            <ul>
                                <li>
                                    <p class="strat_from">Starting Form : </p>
                                    <br>
                                    <p>KES '. $accomodation_price .'/ <span>Night</span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="vk-dark-our-room-item-book-right">
                            <a href="#"> BOOK NOW <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="item">
            <div class="col-md-6 vk-dark-our-room-item-left  vk-clear-padding">
                <div class="vk-dark-our-room-item-img">
                    <a href="#"><img src="' . $imageSrc[0] .'" alt="' . $atts['title'] . '" class="img-responsive"/></a>
                </div>
            </div>
            <div class="col-md-6 vk-dark-our-room-item-right">
                <div class="vk-dark-our-room-item-content">
                    <h3><a href="#">'. $accomodation_title .'</a></h3>
                    <br>
                    <p>' . $accomodation_desc . '</p>
                    <ul>
                        <li><p><i class="fa fa-bed" aria-hidden="true"></i> Bed <span> : '.  $accomodation_bed .'</span></p></li>
                        <li><p><i class="fa fa-binoculars" aria-hidden="true"></i> View <span> : '. $accomodation_view .'</span></p></li>
                        <li><p><i class="fa fa-arrows-alt" aria-hidden="true"></i> Size <span> : '. $accomodation_size .' m2</span></p></li>
                        <li><p><i class="fa fa-coffee" aria-hidden="true"></i> Breakfast <span> : '. $accomodation_breakfirst .'</span></p></li>
                        <li><p><i class="fa fa-users" aria-hidden="true"></i> Max Occupancy: <span> : '. $accomodation_max .'</span></p></li>
                    </ul>
                    <div class="vk-dark-our-room-item-book">
                        <div class="vk-dark-our-room-item-book-left">
                            <ul>
                                <li>
                                    <p class="strat_from">Starting Form : </p>
                                    <br>
                                    <p>KES '. $accomodation_price .'/ <span>Night</span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="vk-dark-our-room-item-book-right">
                            <a href="#"> BOOK NOW <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
        
        ';

        /*<div class="vc-infobox-text">' . $text . '</div>*/   
         
        return $html;
         
    }
     
} // End Element Class
 
 
// Element Class Init
new vcSliderBox();
