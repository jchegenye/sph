<?php
/**
 * SPH functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SPH
 */

if ( ! function_exists( 'sph_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sph_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on SPH, use a find and replace
		 * to change 'sph' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sph', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main-menu-left' => esc_html__( 'Primary Left', 'sph' ),
			'main-menu-right' => esc_html__( 'Primary Right', 'sph' ),
			'footer-menu' => esc_html__( 'Footer Menu Right', 'sph' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'sph_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'sph_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sph_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'sph_content_width', 640 );
}
add_action( 'after_setup_theme', 'sph_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sph_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sph' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sph' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'SPH Copyright', 'sph' ),
		'id'            => 'copyright-1',
		'description'   => __( 'Add widgets here to appear in your footer copyright section.', 'sph' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'SPH Social Media Icons Links', 'sph' ),
		'id'            => 'social-media-1',
		'description'   => __( 'Add social media icons links here to appear in your footer section.', 'sph' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'SPH Company Address', 'sph' ),
		'id'            => 'company-address-1',
		'description'   => __( 'Add company address here to appear in your footer section.', 'sph' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'SPH Company Footer Logo', 'sph' ),
		'id'            => 'company-logo-1',
		'description'   => __( 'Add company footer logo here to appear in your footer section.', 'sph' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'sph_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sph_scripts() {
	wp_enqueue_style( 'sph-style', get_stylesheet_uri() );

	//Stylesheet
	wp_enqueue_style( 'sph-bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ));
	wp_enqueue_style( 'sph-font-awesome', get_theme_file_uri( '/assets/fonts/font-awesome/css/font-awesome.css' ));
	wp_enqueue_style( 'sph-lightgallery', get_theme_file_uri( '/assets/css/lightgallery.css' ));
	wp_enqueue_style( 'sph-owl-carousel', get_theme_file_uri( '/assets/plugin/dist/assets/owl.carousel.min.css' ));
	wp_enqueue_style( 'sph-owl-theme-default', get_theme_file_uri( '/assets/plugin/dist/assets/owl.theme.default.min.css' ));

	wp_enqueue_style( 'sph-bootstrap-theme', get_theme_file_uri( '/assets/css/bootstrap-theme.min.css' ));
	wp_enqueue_style( 'sph-raleway', get_theme_file_uri( '/assets/fonts/raleway/raleway.css' ));
	wp_enqueue_style( 'sph-playfair-display', get_theme_file_uri( '/assets/fonts/playfair-display/playfair-display.css' ));
	wp_enqueue_style( 'sph-themify-icons', get_theme_file_uri( '/assets/css/themify-icons.css' ));
	wp_enqueue_style( 'sph-animate', get_theme_file_uri( '/assets/css/animate.css' ));

	wp_enqueue_style( 'sph-bootstrap-datepicker3', get_theme_file_uri( '/assets/css/bootstrap-datepicker3.css' ));
	wp_enqueue_style( 'sph-jquery-ui.min', get_theme_file_uri( '/assets/css/jquery-ui.min.css' ));
	wp_enqueue_style( 'sph-nice-select', get_theme_file_uri( '/assets/css/nice-select.css' ));
	wp_enqueue_style( 'sph-bootstrap-datetimepicker-min', get_theme_file_uri( '/assets/css/bootstrap-datetimepicker.min.css' ));
	wp_enqueue_style( 'sph-semantic', get_theme_file_uri( '/assets/css/semantic.css' ));

	//SCRIPTS
	wp_enqueue_script( 'sph-jquery', get_theme_file_uri( '/assets/js/jquery.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-jquery1', get_theme_file_uri( '/assets/js/jquery1.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-bootstrap-min', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-main', get_theme_file_uri( '/assets/js/main.js' ), array(), '' );
	wp_enqueue_script( 'sph-lightgallery', get_theme_file_uri( '/assets/js/lightgallery.js' ), array(), '' );
	wp_enqueue_script( 'sph-counterup', get_theme_file_uri( '/assets/js/number-count/jquery.counterup.min.js' ), array(), '' );

	wp_enqueue_script( 'sph-owl-carousel', get_theme_file_uri( '/assets/plugin/dist/owl.carousel.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-isotope-pkgd', get_theme_file_uri( '/assets/js/isotope.pkgd.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-moment', get_theme_file_uri( '/assets/js/moment.min.js' ), array(), '' );

	wp_enqueue_script( 'sph-jquery-waypoints', get_theme_file_uri( '/assets/js/jquery.waypoints.js' ), array(), '' );
	wp_enqueue_script( 'sph-counterup', get_theme_file_uri( '/assets/js/number-count/jquery.counterup.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-jquery-ui', get_theme_file_uri( '/assets/js/jquery-ui.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-bootstrap-datepicker', get_theme_file_uri( '/assets/js/bootstrap-datepicker.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-bootstrap-datepicker-tr', get_theme_file_uri( '/assets/js/bootstrap-datepicker.tr.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-picturefill', get_theme_file_uri( '/assets/js/picturefill.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-lg-pager', get_theme_file_uri( '/assets/js/lg-pager.js' ), array(), '' );
	wp_enqueue_script( 'sph-lg-autoplay', get_theme_file_uri( '/assets/js/lg-autoplay.js' ), array(), '' );
	wp_enqueue_script( 'sph-lg-fullscreen', get_theme_file_uri( '/assets/js/lg-fullscreen.js' ), array(), '' );
	wp_enqueue_script( 'sph-lg-zoom', get_theme_file_uri( '/assets/js/lg-zoom.js' ), array(), '' );

	wp_enqueue_script( 'sph-lg-hash', get_theme_file_uri( '/assets/js/lg-hash.js' ), array(), '' );
	wp_enqueue_script( 'sph-lg-share', get_theme_file_uri( '/assets/js/lg-share.js' ), array(), '' );
	wp_enqueue_script( 'sph-jquery-nice-select', get_theme_file_uri( '/assets/js/jquery.nice-select.js' ), array(), '' );
	wp_enqueue_script( 'sph-semantic', get_theme_file_uri( '/assets/js/semantic.js' ), array(), '' );

	wp_enqueue_script( 'sph-parallax-min', get_theme_file_uri( '/assets/js/parallax.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-jquery-nicescroll', get_theme_file_uri( '/assets/js/jquery.nicescroll.min.js' ), array(), '' );
	wp_enqueue_script( 'sph-jquery-sticky', get_theme_file_uri( '/assets/js/jquery.sticky.js' ), array(), '' );
	wp_enqueue_script( 'sph-bootstrap-datetimepicker', get_theme_file_uri( '/assets/js/bootstrap-datetimepicker.js' ), array(), '' );
	wp_enqueue_script( 'sph-bootstrap-datetimepicker-fr', get_theme_file_uri( '/assets/js/bootstrap-datetimepicker.fr.js' ), array(), '' );
	wp_enqueue_script( 'sph-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'sph-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sph_scripts' );



// Custom Visual Composer Links
add_action( 'vc_before_init', 'vc_before_init_actions' );
function vc_before_init_actions() {
 
    // Require New Visual Composer Element
    require_once( get_template_directory().'/vc-custom-elements/vc-sample-element.php' ); 

    require_once( get_template_directory().'/vc-custom-elements/vc-slider-box-element.php' );

    //require_once( get_template_directory().'/vc-custom-elements/vc-testimonials-element.php' );
 
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Custom Post Types.
 */
require_once( get_template_directory().'/inc/sph-functions/cpt-bookings.php' );
require_once( get_template_directory().'/inc/sph-functions/cpt-reservations.php' );

require_once( get_template_directory().'/inc/sph-functions/cpt-testimonials.php' );
require_once( get_template_directory().'/inc/sph-functions/cpt-gallery.php' );

/**
* WP Bootstrap 3 Navwalker 
*/
if ( ! file_exists( get_template_directory() . '/assets/plugin/dist/wp-bootstrap-navwalker-3-branch/wp-bootstrap-navwalker.php' ) ) {
	// file does not exist... return an error.
	return new WP_Error( 'wp-bootstrap-navwalker-missing', __( 'It appears the wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
	// file exists... require it.
    require_once get_template_directory() . '/assets/plugin/dist/wp-bootstrap-navwalker-3-branch/wp-bootstrap-navwalker.php';
}