<?php

// Register Custom Post Type
function SPH_Reservations() {

	$labels = array(
		'name'                  => _x( 'SPH Reservations', 'Post Type General Name', 'sph' ),
		'singular_name'         => _x( 'SPH Reservation', 'Post Type Singular Name', 'sph' ),
		'menu_name'             => __( 'SPH Reservations', 'sph' ),
		'name_admin_bar'        => __( 'SPH Reservation', 'sph' ),
		'archives'              => __( 'Item Archives', 'sph' ),
		'attributes'            => __( 'Item Attributes', 'sph' ),
		'parent_item_colon'     => __( 'Parent Item:', 'sph' ),
		'all_items'             => __( 'All Items', 'sph' ),
		'add_new_item'          => __( 'Add New Item', 'sph' ),
		'add_new'               => __( 'Add New', 'sph' ),
		'new_item'              => __( 'New Item', 'sph' ),
		'edit_item'             => __( 'Edit Item', 'sph' ),
		'update_item'           => __( 'Update Item', 'sph' ),
		'view_item'             => __( 'View Item', 'sph' ),
		'view_items'            => __( 'View Items', 'sph' ),
		'search_items'          => __( 'Search Item', 'sph' ),
		'not_found'             => __( 'Not found', 'sph' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sph' ),
		'featured_image'        => __( 'Featured Image', 'sph' ),
		'set_featured_image'    => __( 'Set featured image', 'sph' ),
		'remove_featured_image' => __( 'Remove featured image', 'sph' ),
		'use_featured_image'    => __( 'Use as featured image', 'sph' ),
		'insert_into_item'      => __( 'Insert into item', 'sph' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'sph' ),
		'items_list'            => __( 'Items list', 'sph' ),
		'items_list_navigation' => __( 'Items list navigation', 'sph' ),
		'filter_items_list'     => __( 'Filter items list', 'sph' ),
	);
	$args = array(
		'label'                 => __( 'SPH Reservation', 'sph' ),
		'description'           => __( 'SPH reservations', 'sph' ),
		'labels'                => $labels,
		'supports'              => array( '' ),
		'taxonomies'            => array( ''), /*'category', 'post_tag'*/
		'hierarchical'          => true,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-admin-home',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'query_var'             => 'sph_reservations',
		'capability_type'       => 'page',
	);
	register_post_type( 'sph_reservations', $args );

}
add_action( 'init', 'SPH_Reservations', 0 );


require_once( get_template_directory().'/inc/sph-functions/mini-functions/sph-reservations-meta-boxes.php' );