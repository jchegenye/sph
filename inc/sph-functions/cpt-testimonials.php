<?php

    /**
     * Author Name: Jackson A. Chegenye
     * Author URL: httsp://jchegenye.me
     * Date: 2018
     */
    
/*Refer here: https://code.tutsplus.com/tutorials/creating-client-testimonials-with-custom-post-types--wp-30263*/

add_action( 'init', 'sph_testimonials_post_type' );
function sph_testimonials_post_type() {
    $labels = array(
        'name' => 'SPH Testimonials',
        'singular_name' => 'SPH Testimonial',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New SPH Testimonial',
        'edit_item' => 'Edit SPH Testimonial',
        'new_item' => 'New SPH Testimonial',
        'view_item' => 'View SPH Testimonial',
        'search_items' => 'Search SPH Testimonials',
        'not_found' =>  'No SPH Testimonials found',
        'not_found_in_trash' => 'No SPH Testimonials in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'sph_testimonials', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 4,
        'supports' => array( 'editor' ),
        'register_meta_box_cb' => 'sph_testimonials_meta_boxes', // Callback function for custom metaboxes
    ) );
}

/*Adding a Metabox*/
    function sph_testimonials_meta_boxes() {
        add_meta_box( 'sph_testimonials_form', 'SPH Testimonial Details', 'sph_testimonials_form', 'sph_testimonials', 'normal', 'high' );
    }
     
    function sph_testimonials_form() {
        $post_id = get_the_ID();
        $testimonial_data = get_post_meta( $post_id, '_testimonial', true );
        $client_name = ( empty( $testimonial_data['client_name'] ) ) ? '' : $testimonial_data['client_name'];
        $source = ( empty( $testimonial_data['source'] ) ) ? '' : $testimonial_data['source'];
        $link = ( empty( $testimonial_data['link'] ) ) ? '' : $testimonial_data['link'];
     
        wp_nonce_field( 'sph_testimonials', 'sph_testimonials' );
        ?>
        <p>
            <label>Client's Name (optional)</label><br />
            <input type="text" value="<?php echo $client_name; ?>" name="testimonial[client_name]" size="40" />
        </p>
        <p>
            <label>Business/Site Name Or Designation (optional)</label><br />
            <input type="text" value="<?php echo $source; ?>" name="testimonial[source]" size="40" />
        </p>
        <p>
            <label>Link (optional)</label><br />
            <input type="text" value="<?php echo $link; ?>" name="testimonial[link]" size="40" />
        </p>
        <?php
    }

/*Saving the Custom Meta*/
    add_action( 'save_post', 'sph_testimonials_save_post' );
    function sph_testimonials_save_post( $post_id ) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;
     
        if ( ! empty( $_POST['sph_testimonials'] ) && ! wp_verify_nonce( $_POST['sph_testimonials'], 'sph_testimonials' ) )
            return;
     
        if ( ! empty( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) )
                return;
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) )
                return;
        }
     
        if ( ! wp_is_post_revision( $post_id ) && 'sph_testimonials' == get_post_type( $post_id ) ) {
            remove_action( 'save_post', 'sph_testimonials_save_post' );
     
            wp_update_post( array(
                'ID' => $post_id,
                'post_title' => 'Testimonial - ' . $post_id
            ) );
     
            add_action( 'save_post', 'sph_testimonials_save_post' );
        }
     
        if ( ! empty( $_POST['testimonial'] ) ) {
            $testimonial_data['client_name'] = ( empty( $_POST['testimonial']['client_name'] ) ) ? '' : sanitize_text_field( $_POST['testimonial']['client_name'] );
            $testimonial_data['source'] = ( empty( $_POST['testimonial']['source'] ) ) ? '' : sanitize_text_field( $_POST['testimonial']['source'] );
            $testimonial_data['link'] = ( empty( $_POST['testimonial']['link'] ) ) ? '' : esc_url( $_POST['testimonial']['link'] );
     
            update_post_meta( $post_id, '_testimonial', $testimonial_data );
        } else {
            delete_post_meta( $post_id, '_testimonial' );
        }
    }

/*Customizing the List View*/
    add_filter( 'manage_edit-sph_testimonials_columns', 'sph_testimonials_edit_columns' );
    function sph_testimonials_edit_columns( $columns ) {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => 'Title',
            'testimonial' => 'Testimonial',
            'testimonial-client-name' => 'Client\'s Name',
            'testimonial-source' => 'Business/Site',
            'testimonial-link' => 'Link',
            'author' => 'Posted by',
            'date' => 'Date'
        );
     
        return $columns;
    }
     
    add_action( 'manage_posts_custom_column', 'sph_testimonials_columns', 10, 2 );
    function sph_testimonials_columns( $column, $post_id ) {
        $testimonial_data = get_post_meta( $post_id, '_testimonial', true );
        switch ( $column ) {
            case 'testimonial':
                the_excerpt();
                break;
            case 'testimonial-client-name':
                if ( ! empty( $testimonial_data['client_name'] ) )
                    echo $testimonial_data['client_name'];
                break;
            case 'testimonial-source':
                if ( ! empty( $testimonial_data['source'] ) )
                    echo $testimonial_data['source'];
                break;
            case 'testimonial-link':
                if ( ! empty( $testimonial_data['link'] ) )
                    echo $testimonial_data['link'];
                break;
        }
    }

/**
 * Display a testimonial
 *
 * @param  int $post_per_page  The number of sph_testimonials you want to display
 * @param  string $orderby  The order by setting  https://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters
 * @param  array $testimonial_id  The ID or IDs of the testimonial(s), comma separated
 *
 * @return  string  Formatted HTML
 */
    function get_testimonial( $posts_per_page = 1, $orderby = 'none', $testimonial_id = null ) {
        $args = array(
            'posts_per_page' => (int) $posts_per_page,
            'post_type' => 'sph_testimonials',
            'orderby' => $orderby,
            'no_found_rows' => true,
        );
        if ( $testimonial_id )
            $args['post__in'] = array( $testimonial_id );
     
        $query = new WP_Query( $args  );
     
        $testimonials = '';

            $testimonials .= '<div class="vk-sparta-testimonial-content">';
                $testimonials .= '<div class="row">';
                    $testimonials .= '<div class="col-md-10 col-md-offset-1">';
                        $testimonials .= '<div id="vk-owl-testimonial" class="vk-owl-one-item owl-carousel owl-theme">';

                                if ( $query->have_posts() ) {
                                    while ( $query->have_posts() ) : $query->the_post();
                                        $post_id = get_the_ID();
                                        $testimonial_data = get_post_meta( $post_id, '_testimonial', true );
                                        $client_name = ( empty( $testimonial_data['client_name'] ) ) ? '' : $testimonial_data['client_name'];
                                        $source = ( empty( $testimonial_data['source'] ) ) ? '' : ' - <span>' . $testimonial_data['source'] . '<span>';
                                        $link = ( empty( $testimonial_data['link'] ) ) ? '' : $testimonial_data['link'];
                                        $cite = ( $link ) ? '<a href="' . esc_url( $link ) . '" target="_blank">' . $client_name . $source . '</a>' : $client_name . $source;

                                        $testimonials .= '<div class="item">';
                                            $testimonials .= '<div class="vk-sparta-star">';
                                                $testimonials .= '<p>';
                                                    $testimonials .= '<span><i class="fa fa-star" aria-hidden="true"></i></span>';
                                                    $testimonials .= '<span><i class="fa fa-star" aria-hidden="true"></i></span>';
                                                    $testimonials .= '<span><i class="fa fa-star" aria-hidden="true"></i></span>';
                                                    $testimonials .= '<span><i class="fa fa-star" aria-hidden="true"></i></span>';
                                                    $testimonials .= '<span><i class="fa fa-star-o" aria-hidden="true"></i></span>';
                                                $testimonials .= '</p>';
                                            $testimonials .= '</div>';

                                            $testimonials .= '<div class="vk-sparta-testimonial-text"><p>' . get_the_content() . '</p></div>';
                                            $testimonials .= '<div class="vk-sparta-testimonial-author"><p>' . $cite . '</div>';
                                        $testimonials .= '</div>';
                             
                                    endwhile;
                                    wp_reset_postdata();
                                }

                        $testimonials .= '</div>';
                    $testimonials .= '</div>';
                $testimonials .= '</div>';
            $testimonials .= '</div>';
     
        return $testimonials;
    }

add_shortcode( 'testimonial', 'testimonial_shortcode' );
/**
 * Shortcode to display sph_testimonials
 *
 * [testimonial posts_per_page="1" orderby="none" testimonial_id=""]
 */
function testimonial_shortcode( $atts ) {
    extract( shortcode_atts( array(
        'posts_per_page' => '2',
        'orderby' => 'none',
        'testimonial_id' => '',
    ), $atts ) );
 
    return get_testimonial( $posts_per_page, $orderby, $testimonial_id );
}