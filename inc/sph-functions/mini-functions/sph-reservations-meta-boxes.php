<?php

	/**
	 * Author Name: Jackson A. Chegenye
	 * Author URL: httsp://jchegenye.me
	 * Date: 2018
	 */

	add_action( 'add_meta_boxes', 'sph_reservation_add_meta_box' );
	 
	if ( ! function_exists( 'sph_reservation_add_meta_box' ) ) {
		/**
		 * Add meta box to page screen
		 *
		 * This function handles the addition of variuos meta boxes to your page or post screens.
		 * You can add as many meta boxes as you want, but as a rule of thumb it's better to add
		 * only what you need. If you can logically fit everything in a single metabox then add
		 * it in a single meta box, rather than putting each control in a separate meta box.
		 *
		 * @since 1.0.0
		 */
		function sph_reservation_add_meta_box() {
			add_meta_box( 'details-metabox', esc_html__( 'Reservation Details', 'sph' ), 'sph_reservation_details', 'sph_reservations', 'normal', 'high' );
		}
	}


	if ( ! function_exists( 'sph_reservation_details' ) ) {
	/**
	 * Meta box render function
	 *
	 * @param  object $post Post object.
	 * @since  1.0.0
	 */
	function sph_reservation_details( $post ) {

		?>
			<div class="form-box" onload='hideTotal()'>

				<form action="reserv_form" id="" onsubmit="return false;">

					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<h4 class="form-title" data-toggle="collapse" data-target="#colapse_1" aria-expanded="true">Reservations Details</h4>
							<div class="collapse" id="colapse_1" >
								
								<div class="form-inner-box">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputEmail4">Check In Date:*</label>
											<input type="date" class="form-control" id="pick_date" name="pickup_date" onchange="cal()">
										</div>
										<div class="form-group col-md-6">
											<label for="inputEmail4">Check Out Date:*</label>
											<input type="date" class="form-control" id="drop_date" name="dropoff_date" onchange="cal()">
										</div>
									</div>

									<div class="form-row  col-md-12">
										<div class="form-check form-check-inline">
										  	<input class="form-check-input radioBtn" name="property_type" type="radio" data-target-id="1" id="inlineCheckbox2" value="Room" checked="true">
										  	<label class="form-check-label" for="inlineCheckbox2"> Room</label>
										
									  		<input class="form-check-input radioBtn" name="property_type" type="radio" data-target-id="2" id="inlineCheckbox1" value="Conference">
										  	<label class="form-check-label" for="inlineCheckbox1"> Conference</label>
										</div>
									</div>

									<div class="form-row"><hr></div>

									<div class="my-div active me_1 " data-target="1">

										<div class="form-row">

											<div class="form-group  col-md-12">
												<div class="form-check form-check-inline">
												  	<input type="radio" name="roomtype" value="Standard Room" class="form-check-input"> Standard Room
													<input type="radio" name="roomtype" value="Excecutive Room" class="form-check-input"> Excecutive Room
													<input type="radio" name="roomtype" value="Deluxe Room" class="form-check-input"> Deluxe Room
													<input type="radio" name="roomtype" value="Family Suite" class="form-check-input"> Family Suite
												</div>
											</div>

											<div class="form-group form-row  col-md-12">
												<select class="form-control" id="exampleFormControlSelect1">
									                <option selected>Room Size</option>
									                <option value="room_size1">Single Occupancy</option>
									                <option value="room_size2">Double Occupancy</option>
									                <option value="room_size3">Twin Standard</option>
								              	</select>
											</div>

											<div class="form-group form-row  col-md-12">
												<select class="form-control" id="exampleFormControlSelect1">
									                <option selected>Room Type</option>
									                <option value="room_bb">BB</option>
									                <option value="room_bo">BO</option>
									                <option value="room_fb">FB</option>
									                <option value="room_hb">HB</option>
								              	</select>
											</div>

											<div class="form-group col-md-6">
												<select class="form-control" id="exampleFormControlSelect1">
									                <option selected>Adults</option>
									                <option value="1">1</option>
									                <option value="2">2</option>
									                <option value="3">3</option>
									                <option value="4">4</option>
									                <option value="5">5</option>
								              	</select>
							              	</div>

								            <div class="form-group col-md-6">
												<select class="form-control" id="exampleFormControlSelect1">
									                <option selected>Children</option>
									                <option value="1">1</option>
									                <option value="2">2</option>
									                <option value="3">3</option>
									                <option value="4">4</option>
									                <option value="5">5</option>
								              	</select>

							            	</div>

											<div class="form-row">
												<div class="form-group col-md-12">
													<label for="inputEmail4">Total Amount:</label>
													<input type="text" class="form-control" id="numdays2" name="numdays" readonly="true">
												</div>
											</div>

											<div class="my-div me_2" data-target="2">
												<h4>Conference</h4>
											</div>
															
										</div>
									</div>

								</div>

							</div>

							<h4 class="form-title" data-toggle="collapse" data-target="#colapse_2" aria-expanded="false">Customer Information</h4>
							<div class="collapse" id="colapse_2" >
								<div class="form-inner-box">

									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputEmail4">First Name:*</label>
											<input type="text" class="form-control" name="reserv_first_name">
										</div>
										<div class="form-group col-md-6">
											<label for="inputEmail4">Second Name:*</label>
											<input type="text" class="form-control" name="reserv_second_name">
										</div>
										<div class="form-group col-md-12">
											<label for="inputEmail4">Email:*</label>
											<input type="text" class="form-control" name="reserv_email">
										</div>

										<div class="form-group col-md-4">
											<label for="inputEmail4">Country:*</label>
											<input type="text" class="form-control" name="reserv_user_email">
										</div>
										<div class="form-group col-md-8">
											<label for="inputEmail4">Address:</label>
											<input type="text" class="form-control" name="reserv_address">
										</div>

										<div class="form-group col-md-4">
											<label for="inputEmail4">City:</label>
											<input type="text" class="form-control" name="reserv_city">
										</div>
										<div class="form-group col-md-4">
											<label for="inputEmail4">Postal Code</label>
											<input type="text" class="form-control" name="reserv_postal_code">
										</div>
										<div class="form-group col-md-4">
											<label for="inputEmail4">Phone Number:*</label>
											<input type="text" class="form-control" name="reserv_phne_no">
										</div>

									</div>

								</div>
								<input type='submit' id='submit' value='Submit' onclick="calculateTotal()" class=" vk-btn" />
							</div>
						</div>
					</div>

		       	</form>

			</div>

		<?php

	}
	add_shortcode( 'reservations_form', 'sph_reservation_details' );

}