<?php

	/**
	 * Author Name: Jackson A. Chegenye
	 * Author URL: httsp://jchegenye.me
	 * Date: 2018
	 */

	add_action( 'add_meta_boxes', 'sph_booking_add_meta_box' );
	 
	if ( ! function_exists( 'sph_booking_add_meta_box' ) ) {
		/**
		 * Add meta box to page screen
		 *
		 * This function handles the addition of variuos meta boxes to your page or post screens.
		 * You can add as many meta boxes as you want, but as a rule of thumb it's better to add
		 * only what you need. If you can logically fit everything in a single metabox then add
		 * it in a single meta box, rather than putting each control in a separate meta box.
		 *
		 * @since 1.0.0
		 */
		function sph_booking_add_meta_box() {
			add_meta_box( 'details-metabox', esc_html__( 'Property Details', 'sph' ), 'sph_booking_details', 'sph_bookings', 'normal', 'high' );
		}
	}

	if ( ! function_exists( 'sph_booking_details' ) ) {
	/**
	 * Meta box render function
	 *
	 * @param  object $post Post object.
	 * @since  1.0.0
	 */
	function sph_booking_details( $post ) {
		$meta = get_post_meta( $post->ID );
		$booking_price = ( isset( $meta['booking_price'][0] ) && '' !== $meta['booking_price'][0] ) ? $meta['booking_price'][0] : '';
		$booking_adults = ( isset( $meta['booking_adults'][0] ) && '' !== $meta['booking_adults'][0] ) ? $meta['booking_adults'][0] : '';
		$booking_kids = ( isset( $meta['booking_kids'][0] ) && '' !== $meta['booking_kids'][0] ) ? $meta['booking_kids'][0] : '';
		$booking_availability = ( isset( $meta['booking_availability'][0] ) &&  '1' === $meta['booking_availability'][0] ) ? 1 : 0;
		wp_nonce_field( 'mytheme_control_meta_box', 'mytheme_control_meta_box_nonce' ); // Always add nonce to your meta boxes!
		?>
		<style type="text/css">
			.post_meta_extras p{margin: 20px;}
			.post_meta_extras label{display:block; margin-bottom: 10px;}
		</style>
		<div class="post_meta_extras">
			<p>
				<label><b><?php esc_attr_e( 'Price', 'sph' ); ?></b></label>
				<input type="text" name="booking_price" value="<?php echo esc_attr( $booking_price ); ?>">
			</p>
			
			<table style="margin-left: 15px;">
				<tr><td colspan="2"><label><b><?php esc_attr_e( 'Number of persons:', 'sph' ); ?></b></label></td></tr>
			 	<tr><td>Adults: </td><td><input type="number" name="booking_adults" value="<?php echo esc_attr( $booking_adults ); ?>"></td></tr>
				<tr><td>Kid(s): </td> <td><input type="number" name="booking_kids" value="<?php echo esc_attr( $booking_kids ); ?>"></td></tr>
			</table>
			
			<p>
				<label><input type="checkbox" name="booking_availability" value="1" <?php checked( $booking_availability, 1 ); ?> /><?php esc_attr_e( 'Availability', 'sph' ); ?> - <b><em>(Manualy Change Status)</em></b></label>
			</p>
			<p><hr></p>
			<p>
				Availability Status: 
				<?php 
					if ($booking_availability == 1) {
						?> <span style="color: red;">Occupied </span> <?php
					}else{
						?> <span style="color: green;">Vacant </span> <?php
					}

				?>
				
			</p>
		</div>
		<?php
	}
}


add_action( 'save_post', 'mytheme_save_metaboxes' );
 
if ( ! function_exists( 'mytheme_save_metaboxes' ) ) {
	/**
	 * Save controls from the meta boxes
	 *
	 * @param  int $post_id Current post id.
	 * @since 1.0.0
	 */
	function mytheme_save_metaboxes( $post_id ) {
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times. Add as many nonces, as you
		 * have metaboxes.
		 */
		if ( ! isset( $_POST['mytheme_control_meta_box_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['mytheme_control_meta_box_nonce'] ), 'mytheme_control_meta_box' ) ) { // Input var okay.
			return $post_id;
		}
 
		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' === $_POST['post_type'] ) { // Input var okay.
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			}
		} else {
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}
		}
 
		/*
		 * If this is an autosave, our form has not been submitted,
		 * so we don't want to do anything.
		 */
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
 
		/* Ok to save */
 
		if ( isset( $_POST['booking_price'] ) ) { // Input var okay.
			update_post_meta( $post_id, 'booking_price', sanitize_text_field( wp_unslash( $_POST['booking_price'] ) ) ); // Input var okay.
		}
		if ( isset( $_POST['booking_adults'] ) ) { // Input var okay.
			update_post_meta( $post_id, 'booking_adults', sanitize_text_field( wp_unslash( $_POST['booking_adults'] ) ) ); // Input var okay.
		}
		if ( isset( $_POST['booking_kids'] ) ) { // Input var okay.
			update_post_meta( $post_id, 'booking_kids', sanitize_text_field( wp_unslash( $_POST['booking_kids'] ) ) ); // Input var okay.
		}
		
 
		$booking_availability = ( isset( $_POST['booking_availability'] ) && '1' === $_POST['booking_availability'] ) ? 1 : 0; // Input var okay.
		update_post_meta( $post_id, 'booking_availability', esc_attr( $booking_availability ) );
 
	}
}