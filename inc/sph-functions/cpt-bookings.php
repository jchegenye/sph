<?php
/*
* CPT for SPH Bookins Post Type

*/
    /**
     * Author Name: Jackson A. Chegenye
     * Author URL: httsp://jchegenye.me
     * Date: 2018
     */

// Register Custom Post Type
function SPH_Bookings() {

    $labels = array(
        'name'                  => _x( 'SPH Bookings', 'Post Type General Name', 'sph' ),
        'singular_name'         => _x( 'SPH Booking', 'Post Type Singular Name', 'sph' ),
        'menu_name'             => __( 'SPH Bookings', 'sph' ),
        'name_admin_bar'        => __( 'SPH Booking', 'sph' ),
        'archives'              => __( 'Property Archives', 'sph' ),
        'attributes'            => __( 'Property Attributes', 'sph' ),
        'parent_property_colon'     => __( 'Parent Property:', 'sph' ),
        'all_properties'             => __( 'All Properties', 'sph' ),
        'add_new_property'          => __( 'Add New Property', 'sph' ),
        'add_new'               => __( 'Add New', 'sph' ),
        'new_property'              => __( 'New Property', 'sph' ),
        'edit_property'             => __( 'Edit Property', 'sph' ),
        'update_property'           => __( 'Update Property', 'sph' ),
        'view_property'             => __( 'View Property', 'sph' ),
        'view_properties'            => __( 'View Properties', 'sph' ),
        'search_properties'          => __( 'Search Property', 'sph' ),
        'not_found'             => __( 'Not found', 'sph' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'sph' ),
        'featured_image'        => __( 'Featured Image', 'sph' ),
        'set_featured_image'    => __( 'Set featured image', 'sph' ),
        'remove_featured_image' => __( 'Remove featured image', 'sph' ),
        'use_featured_image'    => __( 'Use as featured image', 'sph' ),
        'insert_into_property'      => __( 'Insert into Property', 'sph' ),
        'uploaded_to_this_property' => __( 'Uploaded to this Property', 'sph' ),
        'properties_list'            => __( 'Properties list', 'sph' ),
        'properties_list_navigation' => __( 'Properties list navigation', 'sph' ),
        'filter_properties_list'     => __( 'Filter Properties list', 'sph' ),
    );
    $args = array(
        'label'                 => __( 'SPH Booking', 'sph' ),
        'description'           => __( 'SPH bookings and reservations', 'sph' ),
        'labels'                => $labels,
        'supports'              => array( 'editor', 'thumbnail', 'excerpt' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => true,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-book',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => false,
        'query_var'             => 'sph_bookings',
        'capability_type'       => 'page',
    );
    register_post_type( 'sph_bookings', $args );

}
add_action( 'init', 'SPH_Bookings', 0 );


/*Customizing the List View*/
add_filter( 'manage_sph_bookings_posts_columns', 'smashing_sph_bookings_columns' );
function smashing_sph_bookings_columns( $columns ) {
  
  
    $columns = array(
      'cb' => $columns['cb'],
      'image' => __( 'Image' ),
      'price' => __( 'Price', 'sph' ),
      'persons' => __( 'No. of Persons', 'sph' ),
      'category' => __( 'Categories', 'sph' ),
      'tags' => __( 'Tags', 'sph' ),
      'date' => __( 'Date', 'sph' ),
      'availability' => __( 'Status', 'sph' ),
    );
  
  
  return $columns;
}


add_action( 'manage_sph_bookings_posts_custom_column', 'smashing_sph_bookings_column', 10, 2);
function smashing_sph_bookings_column( $column, $post_id ) {
  // Image column
  if ( 'image' === $column ) {
    echo get_the_post_thumbnail( $post_id, array(80, 80) );
  }

  // Monthly price column
    if ( 'price' === $column ) {
        $price = get_post_meta( $post_id, 'booking_price', true );

        if ( ! $price ) {
          _e( 'n/a' );  
        } else {
          echo 'KES ' . number_format( $price, 0, '.', ',' ) . ' ';
        }
    }

    if ( 'price' === $column ) {

        $tags = get_the_tags( $post->ID );
        $separator = '/';
        $output = '';

        if($tags){

            if ( ! $tags ) {

                  _e( 'n/a' ); 

                }else{

                    foreach($tags as $tag) {

                        if($tag->slug == "bb" OR $tag->slug == "bo" OR $tag->slug == "fb" OR $tag->slug == "hb"){

                            /*$get_split = str_split($tag->name, 2);
                            foreach ($get_split as $key => $value) {*/
                                $output .= strtolower($tag->name) ;
                            /*}*/
                        }

                    }
                echo trim($output, $separator);
            }
        }

    }

    if ( 'persons' === $column ) {
        $adults = get_post_meta( $post_id, 'booking_adults', true );
    
        if ( ! $adults ) {
          _e( 'n/a' );  
        } else {
          echo $adults . ' adult(s)' . ', ';
        }

        $kids = get_post_meta( $post_id, 'booking_kids', true );
    
        if ( ! $kids ) {
          _e( 'n/a' );  
        } else {
          echo $kids . ' kid(s)';
        }
    }

    if ( 'category' === $column ) {

        $categories = get_the_category( $post->ID );
        $separator = ',';
        $output = '';

        if ( ! $categories ) {
          _e( 'n/a' );  
        }else{

            if($categories){

                foreach($categories as $category) {
                    if($category->slug != "yourtag"){
                       $output .= $category->name.$separator;
                    }
                }
                echo trim($output, $separator);
            }
        }
    }

    if ( 'availability' === $column ) {
        $availability = get_post_meta( $post_id, 'booking_availability', true );

        if ($availability == 1) {
            ?> <span style="color: red;"><b>Occupied</b> </span> <?php
        }else{
            ?> <span style="color: green;"><b>Vacant</b> </span> <?php
        }
    }

}


require_once( get_template_directory().'/inc/sph-functions/mini-functions/sph-booking-meta-boxes.php' );