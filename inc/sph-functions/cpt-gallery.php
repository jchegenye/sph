<?php
    /**
     * Author Name: Jackson A. Chegenye
     * Author URL: httsp://jchegenye.me
     * Date: 2018
     */

add_action( 'init', 'add_gallery_post_type' );
function add_gallery_post_type() {
    register_post_type( 'zm_gallery',
        array(
            'labels' => array(
                'name' => __( 'Gallery' ),
                'singular_name' => __( 'Gallery' ),
                'all_items' => __( 'All Images')
            ),
            'public' => true,
            'has_archive' => false,
            'exclude_from_search' => true,
            'rewrite' => array('slug' => 'gallery-item'),
            'supports' => array( 'title', 'thumbnail' ),
            'menu_position' => 4,
            'show_in_admin_bar'   => false,
            'show_in_nav_menus'   => false,
            'publicly_queryable'  => false,
            'query_var'           => false
        )
    );
}


function zm_get_backend_preview_thumb($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
        return $post_thumbnail_img[0];
    }
}

function zm_preview_thumb_column_head($defaults) {
    $defaults['featured_image'] = 'Image';
    return $defaults;
}
add_filter('manage_posts_columns', 'zm_preview_thumb_column_head');

function zm_preview_thumb_column($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = zm_get_backend_preview_thumb($post_ID);
            if ($post_featured_image) {
                echo '<img src="' . $post_featured_image . '" />';
            }
    }
}
add_action('manage_posts_custom_column', 'zm_preview_thumb_column', 10, 2);


add_shortcode( 'show_sph_gallery', 'sph_gallery_shortcode' );


function sph_gallery_shortcode(){

    $args = array (
        'post_type' => 'zm_gallery',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $query = new WP_Query( $args ); ?>
    
        <div class="clearfix"></div>
        <div class="vk-sparta-center-logo-image-gallery">
            <div class="vk-sparta-image-gallery">
                <div class="vk-sparta-image-gallery-img">
                    <div id="lightgallery">

                        <?php 
                            // The Loop
                            if ( $query->have_posts() ) :
                                
                                while ( $query->have_posts() ) : $query->the_post();
                                    
                                    if (has_post_thumbnail()) { 

                                        ?>

                                            <div class="col-md-3 col-md-6 col-xs-6 vk-clear-padding" data-src="<?php the_post_thumbnail_url(); ?>">
                                                <div class="vk-gallery-item-img">
                                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive">

                                                    <div class="vk-item-caption">
                                                        <div class="featured-slider-overlay"></div>
                                                        <div class="vk-item-caption-text">
                                                            <h2>Lorem Ipsum</h2>
                                                            <h4>Restaurant</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        <?php

                                    }   
                                
                                endwhile;

                            endif;
                        ?>

                    </div>
                </div>
                <div class="vk-sparta-image-gallery-text">
                    <span><a href="02_03_gallery_grid_full_width.html">GALLERY</a></span>
                </div>
            </div>
        </div>
<?php
}