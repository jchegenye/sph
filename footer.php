<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SPH
 */

do_shortcode('[show_sph_gallery]');

?>

				</div>
				
			</div><!-- 3 Div -->
		

			<footer class="site-footer footer-default">
	            <div class="footer-main-content">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-md-3">
	                            <div class="widget-title">
	                                <a href="#">
	                                	<?php 
	                                		if(is_active_sidebar('company-logo-1')){

	                                			dynamic_sidebar('company-logo-1');

                                			}else{ ?>

                                				<img src="images/savannah/white-logo.png" alt="" class="img-responsive">

                                			<?php
                                		} ?>
	                                	
	                                </a>
	                            </div>
	                        </div>
	                        <div class="col-md-9">
	                            <h1 class="footer-tagline">comfort redefined</h1>
	                        </div>
	                    </div>

	                    <div class="row">
	                        <div class="col-md-12">
	                            <hr/>
	                        </div>
	                    </div>

	                    <div class="row">
	                        <div class="footer-main-content-elements">
	                            <div class="footer-main-content-element col-sm-4">
	                                <aside class="widget">
	                                    <div class="vk-widget-footer-1">
	                                        
	                                        <div class="widget-content">

	                                            <div class="vk-widget-content-info">

	                                            	<?php 
	                                            		if(is_active_sidebar('company-address-1')){

															dynamic_sidebar('company-address-1');

														}else{ ?>

															<p><span class="ti-location-pin"></span> MAKINDU</p>
	                                                		<p><span class="ti-mobile"></span> +254 700 000 000</p>
	                                                		<p><span class="ti-email"></span> hello@savannahparadise.com</p>

														<?php }

	                                            	?>

	                                            </div>
	                                            <div class="vk-widget-content-share">
	                                                
                                                	<?php 

	                                                	if(is_active_sidebar('social-media-1')){

															dynamic_sidebar('social-media-1');

														}else{ ?>

															<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
		                                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>

														<?php }
                                     
                                            		?>
	                                                
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="clearfix"></div>

	                                </aside>
	                            </div>
	                            <div class="footer-main-content-element col-sm-2">
	                                <aside class="widget">
	                                    <div class="vk-widget-footer-2">
	                                        <div class="widget-content">
	                                            <ul class="vk-widget-content-2">

	                                                <ul class="copyright-menu">
					                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
					                                    <?php wp_nav_menu( array( 'theme_location' => 'main-menu-left' ) ); ?>
					                                </ul>
	                                                
	                                            </ul>
	                                        </div>
	                                    </div>
	                                </aside>
	                            </div>
	                            <div class="footer-main-content-element col-sm-2">
	                                <aside class="widget">
	                                    <div class="vk-widget-footer-3">
	                                        <div class="widget-content">
	                                            <ul class="vk-widget-content-2">
	                                            	<i class="fa fa-angle-right" aria-hidden="true"></i>
	                                            	<?php wp_nav_menu( array( 'theme_location' => 'main-menu-right' ) ); ?>
	                                                <!-- <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> About</a></li>
	                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Gallery</a></li>
	                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Location</a></li>
	                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Contact Us</a></li> -->
	                                            </ul>
	                                        </div>
	                                    </div>
	                                </aside>
	                            </div>
	                            <div class="footer-main-content-element col-sm-4">
	                                <aside class="widget">
	                                    <div class="vk-widget-footer-4">
	                                        <div class="widget-content">
	                                            <div class="form-group">
	                                                <div class="input-group">
	                                                    <input type="email" class="form-control" placeholder="Your Email...">
	                                                    <span class="input-group-addon success"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
	                                                </div>
	                                            </div>
	                                            <div class="vk-widget-trip">
	                                                <a href="#"><!-- <img src="images/01_01_default/stripad.png" alt=""> --></a>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </aside>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="copyright-area">
	                <div class="container">
	                    <div class="copyright-content">
	                        <div class="row">
	                            <div class="col-sm-6">
	                                <div class="copyright-text">
	                                    <div class="copyby">Powered by</div> &nbsp; 

	                                    	<?php 

		                                    	if(is_active_sidebar('social-media-1')){

		                                    		dynamic_sidebar('copyright-1'); 

	                                    		}else{ ?>

	                                    			<a href="<?php echo esc_url( __( 'http://jchegenye.me', 'sph' ) ); ?>"> Maliwatt </a>

	                                    		<?php }

	                                		?>

	                                </div>
	                            </div>
	                            <div class="col-sm-6">
	                                <ul class="copyright-menu">
	                                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
	                                </ul>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </footer>

		</div><!-- 1 Div -->
	</div><!-- 2 Div -->

<?php wp_footer(); ?>

</body>
</html>
