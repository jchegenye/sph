<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SPH
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!--load page-->
	<div class="load-page">
	    <div class="spinner">
	        <div class="rect1"></div>
	        <div class="rect2"></div>
	        <div class="rect3"></div>
	        <div class="rect4"></div>
	        <div class="rect5"></div>
	    </div>
	</div>

	<div class="vk-sparta-center-logo"><!-- 1st Div -->

		<div id="wrapper-container" class="site-wrapper-container"><!-- 2nd Div -->

			<header class="site-header header-default header-sticky ">
		        <nav class="main-navigation ">

		            <div class="vk-main-menu animated uni-sticky"><!-- uni-sticky -->
		                <div class="container">
		                    <div class="vk-center-logo-menu">
		                        <div class="vk-navbar-header navbar-header">
		                            <div class="vk-divider left hidden-xs hidden-sm"></div>
		                            <div class="vk-divider right hidden-xs hidden-sm"></div>
		                            <a class="vk-navbar-brand navbar-brand" href="<?php echo home_url(); ?>">
		                                
		                                <img src="<?php echo get_theme_mod( 'custom-logo' ); ?>" class="img-responsive">
		                                <?php

											$custom_logo_id = get_theme_mod( 'custom_logo' );
											$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
											if ( has_custom_logo() ) {
											        echo '<img src="'. esc_url( $logo[0] ) .'" class="img-responsive">';
											} else {
											        echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
											}
												
										?>
		                            </a>
		                            <!--./vk-navbar-brand-->
		                        </div>
		                        <!--./vk-navbar-header-->

		                        <div class="row">
		                            <nav class="main-navigation">
		                                <div class="inner-navigation">

		                                	<?php
										        wp_nav_menu( array(
										            'theme_location'    => 'main-menu',
										            'depth'             => 2,
										            'container'         => 'div',
										            'container_class'   => '',
										            'container_id'      => '',
										            'menu_class'        => 'navbar-nav vk-navrbar-left',
										            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
										            'walker'            => new WP_Bootstrap_Navwalker())
										        );

										         wp_nav_menu( array(
										            'theme_location'    => 'main-menu-right',
										            'depth'             => 2,
										            'container'         => 'div',
										            'container_class'   => '',
										            'container_id'      => '',
										            'menu_class'        => 'navbar-nav vk-navrbar-right',
										            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
										            'walker'            => new WP_Bootstrap_Navwalker())
										        );
									        ?>
		                                    <div class="clearfix"></div>
		                                </div>
		                            </nav>
		                        </div>
		                        <!--./vk-navbar-collapse-->

		                        <!--shortcode-->
		                        <div class="show-hover-shortcodes animated">
		                            <div class="row">
		                                <div class="col-md-3">
		                                    <h4>SHORT CODE 1</h4>
		                                    <ul>
		                                        <li><a href="08_01_buttons.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Buttons</a></li>
		                                        <li><a href="08_02_accordion.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Accordion</a></li>
		                                        <li><a href="08_03_notification_box.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Notification</a></li>
		                                    </ul>
		                                </div>
		                                <div class="col-md-3">
		                                    <h4>SHOTR CODE 2</h4>
		                                    <ul>
		                                        <li><a href="08_04_icon_box.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Icon Box</a></li>
		                                        <li><a href="08_05_tabs.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Tabs</a></li>
		                                        <li><a href="08_06_typography.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Typography</a></li>
		                                    </ul>
		                                </div>
		                                <div class="col-md-3">
		                                    <h4>SHOTR CODE 3</h4>
		                                    <ul>
		                                        <li><a href="08_07_dividers.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Dividers</a></li>
		                                        <li><a href="08_08_icon.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Icon</a></li>
		                                        <li><a href="08_09_color.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Color</a></li>
		                                    </ul>
		                                </div>
		                                <div class="col-md-3">
		                                    <h4>SHOTR CODE 4</h4>
		                                    <ul>
		                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Dropdown Menu</a></li>
		                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Mega Menu</a></li>
		                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Search Box</a></li>
		                                    </ul>
		                                </div>
		                                <div class="clearfix"></div>
		                            </div>
		                        </div>
		                        <!--search-->
		                        <div class="box-search-header collapse in" id="box-search-header">
		                            <div class="vk-input-group">
		                                <div class="input-group stylish-input-group">
		                                    <input type="text" class="form-control"  placeholder="Type keywords.." >
		                                    <span class="input-group-addon">
		                                    <button type="submit">
		                                        <i class="fa fa-search" aria-hidden="true"></i>
		                                    </button>
		                                </span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </nav>
		    </header>

				<div id="main-content" class="site-main-content"><!-- 3rd Div -->

					<div id="home-main-content" class="site-home-main-content">