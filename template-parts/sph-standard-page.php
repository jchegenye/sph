<?php /* Template Name: SPH Standard Page */

get_header(); ?>
	
	<!-- Background Image Header -->
	<div class="vk-gallery-grid-full-banner">
        <div class="vk-about-banner">
            <div class="vk-about-banner-destop">
                <div class="vk-banner-img" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>')">
	                <div class="vk-about-banner-caption">
	                    <div class="container">
	                        <h2 class="animated fadeInUp slide-delay-2"><?php echo the_title(); ?></h2>
	                        <div class="clearfix"></div>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

	<!-- Main Conent -->
	<div class="container">
		<div class="row">

            <?php
            while ( have_posts() ) : the_post();

                the_content(); // Will output the content of visual composer

            endwhile; // End of the loop.
            ?>

		</div>
	</div>

<?php

get_footer();