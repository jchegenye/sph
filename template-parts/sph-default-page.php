<?php /* Template Name: SPH Default Page */

get_header(); ?>
		
	<!-- SPH Main Slider -->
	<div class="container-fluid">
		<div class="row">
			<?php echo do_shortcode('[rev_slider alias="SPH Main Slider"]'); ?>
		</div>
	</div>

	<div class="vk-booking-center-logo">
        <div class="container">
            <form action="action.php" class="form-horizontal  booking-hotel-all">
                <ul>
                    <li class="filter-box">
                        <h4>Check - In</h4>
                        <div class="input-group date date-check-in" data-date="12-02-2017" data-date-format="mm-dd-yyyy">
                            <input name="date1" class="form-control" type="text" value="12-02-2017">
                            <span class="input-group-addon btn"><span class="ti-calendar" id="ti-calendar1"></span></span>
                        </div>
                    </li>
                    <li class="filter-box">
                        <h4>Check - Out</h4>
                        <div class="input-group date date-check-out" data-date="12-02-2017" data-date-format="mm-dd-yyyy">
                            <input name="date2" class="form-control" type="text" value="12-02-2017">
                            <span class="input-group-addon btn"><span class="ti-calendar" id="ti-calendar2"></span></span>
                        </div>
                    </li>
                    <li>

                        <table>
                            <tr>
                                <td>

                                    <div class="filter-box-plus-minus">
                                        <h4 class="filter-title">Adults</h4>
                                    
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                          <button type="button" class="btn btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                              <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                            </button>
                                            </span>
                                            <input type="text" name="quant[1]" class="form-control input-number filter-value" value="1" min="1" max="10">
                                            <span class="input-group-btn">
                                          <button type="button" class="btn btn-number" data-type="plus" data-field="quant[1]">
                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </button>
                                            </span>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <tr><td><hr/></td></tr>

                            <tr>
                                <td>

                                    <div class="filter-box-plus-minus">
                                        
                                        <h4 class="filter-title">Children</h4>

                                        <div class="input-group">
                                            <span class="input-group-btn">
                                          <button type="button" class="btn btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                              <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                            </button>
                                            </span>
                                            <input type="text" name="quant[1]" class="form-control input-number filter-value" value="1" min="1" max="10">
                                            <span class="input-group-btn">
                                          <button type="button" class="btn btn-number" data-type="plus" data-field="quant[1]">
                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </button>
                                            </span>
                                        </div>
                                            
                                    </div>
                                </td>
                            </tr>

                        </table>

                    </li>
                    <li class="filter-box-submit">
                        <div class="vk-btn-check">
                            <button type="submit" class="btn vk-btn-primary btn-block btn-check">Check  Availability</button>
                        </div>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

	<!-- Main Conent -->
	<div class="container">
		<div class="row">

            <?php
            while ( have_posts() ) : the_post();

                the_content(); // Will output the content of visual composer

            endwhile; // End of the loop.
            ?>

		</div>
	</div>

<?php

get_footer();